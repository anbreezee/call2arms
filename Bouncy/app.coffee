bouncy = require 'bouncy'
server = bouncy (req, res, bounce) ->
    if req.headers.host is 'api.call2arms.net'
        bounce 8080
    else if req.headers.host is 'reply.call2arms.net'
        bounce 8081
    else if req.headers.host is 'kue.call2arms.net'
        bounce 3000
    else 
        bounce 8008
server.listen 8082