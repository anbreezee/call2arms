Q = require 'q'

async_func = (id, cb) ->
	cb null, id

func = (id) ->
	deferred = Q.defer()
	async_func id, (err, value) ->
		deferred.reject 'common'
	deferred.promise

func2 = (id) ->
	deferred = Q.defer()
	async_func id, (err, value) ->
		deferred.resolve 'second: ' + value
	deferred.promise

func('ok')
	.then(
		(value) ->
			console.log 'val1: ' + value
			return func2(value)
	)
	.then(
		(value) ->
			console.log 'val2: ' + value
			return func2(value)
		(reason) ->
			throw 500
	)
	.then(
		(value) ->
			console.log 'val3: ' + value
	)
	.fail(
		(reason) ->
			console.log reason
	)