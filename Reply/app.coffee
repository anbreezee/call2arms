http = require 'http'
faye = require 'faye'

PORT = process.env.PORT || 8081

bayeux = new faye.NodeAdapter {mount: '/', timeout: 45}

# Handle non-Bayeux requests
server = http.createServer (req, res) ->
	res.writeHead 200, {'Content-Type': 'text/plain'}
	res.end 'Hello, non-Bayeux request'

secret = 'sj!k~lj4jgldfj0a9vjfsj4###oijgijoejboi5789jf0sgui043ivjoau49uvakfkfifghio';

bayeux.addExtension {
	incoming: (message, callback) ->
		if not message.channel.match(/^\/meta\//)
			password = message.ext && message.ext.password
			if password isnt secret then message.error = '403::Password required'
		callback message
	outgoing: (message, callback) ->
		if message.ext then delete message.ext.password
		callback message
}

bayeux.attach server
server.listen PORT
console.log 'Reply server listening on port ' + PORT
