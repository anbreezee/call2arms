$(function() {

	var API_HOST = 'http://api.call2arms.net/v1';

	$('#post_btn').click(function() {
		createCallSign();
	});

	$('#get_btn').click(function() {
		getCallSign();
	});

	$('#put_btn').click(function() {
		updateCallSign();
	});

	$('#del_btn').click(function() {
		releaseCallSign();
	});

	$('#response_btn').click(function() {
		sendResponse();
	});

	$('#send_request_btn').click(function() {
		sendRequestCallsign();
	});

	$('#get_request_btn').click(function() {
		getRequestCallsign();
	});

	var fillJson = function(json) {
		for (var key in json) {
			if (json.hasOwnProperty(key)) {
				json[key] = $('#' + json[key]).val();
			}
		}
		return json;
	}

	var sendRequest = function(type, url, data) {
		url = API_HOST + url;
		console.log('> Request sends to: ' + type + ' ' + url);

		if (type == 'DELETE') {
			var params = Array();
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					params.push(key + '=' + encodeURIComponent(data[key]));
				}
			}
			params = params.join('&');
			url += '?' + params;
		}

		$.ajax({type: type, url: url, data: data})
			.done(function(msg) {
				console.log('< Response:');
	    		console.log(msg);
	  		});
	}

	var createCallSign = function() {
		console.log('> Create CallSign');
		var json = fillJson({
			'callsign': 'post_callsign',
			'callbackUrl': 'post_callbackUrl'
		});
		console.log('# Args:');
		console.log(json);
		sendRequest('POST', '/callsign', json);
		return false;
	}

	var getCallSign = function() {
		console.log('> Get CallSign');
		var json = fillJson({
			'callsign': 'get_callsign',
			'authkey': 'get_authkey'
		});
		console.log('# Args:');
		console.log(json);
		sendRequest('GET', '/callsign/' + json.callsign, json);
		return false;
	}

	var updateCallSign = function() {
		console.log('> Update CallSign');
		var json = fillJson({
			'callsign': 'put_callsign',
			'authkey': 'put_authkey',
			'mode': 'put_mode',
			'callbackUrl': 'put_callbackUrl'
		});
		console.log('# Args:');
		console.log(json);
		sendRequest('PUT', '/callsign/' + json.callsign, json);
		return false;
	}

	var releaseCallSign = function() {
		console.log('> Release CallSign');
		var json = fillJson({
			'callsign': 'del_callsign',
			'authkey': 'del_authkey'
		});
		console.log('# Args:');
		console.log(json);
		sendRequest('DELETE', '/callsign/' + json.callsign, json);
		return false;
	}

	var sendResponse = function() {
		console.log('> Send response');
		var json = fillJson({
			'callsign': 'response_callsign',
			'authkey': 'response_authkey',
			'actionId': 'response_actionId',
			'message': 'response_message'
		});
		console.log('# Args:');
		console.log(json);
		sendRequest('POST', '/response', json);
		return false;
	}

	var sendRequestCallsign = function() {
		console.log('> Send request');
		var json = fillJson({
			'callsign': 'send_request_callsign',
			'actionId': 'send_request_actionId',
			'message': 'send_request_message'
		});
		console.log('# Args:');
		console.log(json);
		sendRequest('POST', '/request', json);
		return false;
	}

	var getRequestCallsign = function() {
		console.log('> Get request');
		var json = fillJson({
			'callsign': 'get_request_callsign'
		});
		console.log('# Args:');
		console.log(json);
		sendRequest('GET', '/request/' + json.callsign, json);
		return false;
	}

})