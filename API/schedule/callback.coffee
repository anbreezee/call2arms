### callback ###
request      = require 'request'
model        = require '../models/callsign'
modelRequest = require '../models/request'

kue          = require 'kue'
jobs         = kue.createQueue()

### Settings ###
REQUEST_TIMEOUT   = 10000 # 10 sec
USER_AGENT        = 'call2arms.net'
VALID_RESPONSE    = '*ok*'

sendPostRequest = (callbackUrl, data, cb) ->
	options = {
		url: callbackUrl
		timeout: REQUEST_TIMEOUT
		method: 'POST'
		form: data
		headers: { 'User-Agent': USER_AGENT }
	}
	request options, (error, response, body) ->
		if error or response.statusCode isnt 200
			return cb false
		isOk = body.trim().toLowerCase() is VALID_RESPONSE
		return cb isOk

exports.createJob = (data) ->
	jobs.create('callback', {title: data.callsign, model: data}).priority('high').attempts(5).save();
	jobs.process 'callback', (job, done) ->
		model.getCallbackUrl job.data.model.callsign, (callbackUrl) ->
			if callbackUrl.status isnt 200 then done && done()
			else
				if callbackUrl.model.callbackUrl is ''
					console.log 'null callback'
				else
					console.log 'callback in progress'
					sendPostRequest callbackUrl.model.callbackUrl, job.data.model, (isOk) ->
						if not isOk
							console.log 'callback not ok'
							return done 'error'
						console.log 'callback ok'
						done && done()
