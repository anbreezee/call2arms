SERVICES_ROOT = '../services/'

uiService 		= require SERVICES_ROOT + 'ui'
callsignService = require SERVICES_ROOT + 'callsign'
responseService = require SERVICES_ROOT + 'response'
requestService  = require SERVICES_ROOT + 'request'

exports.setRoutes = (app, auth) ->
	app.use '/v1/ui', new uiService()
	app.use '/v1/callsign', new callsignService()
	app.use '/v1/response', new responseService()
	app.use '/v1/request', new requestService()