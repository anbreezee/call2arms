feathers    = require 'feathers'
routes      = require './routes'

app = module.exports = feathers()
#app.configure(feathers.socketio());

### Secure credentials ###

passlock = false
username = ''
password = ''

auth = null
if passlock
	auth = feathers.basicAuth(
		(user, pass) ->
			user is username and pass is password
		'Secret Area'
	);


### Configuration ###

app.set 'port', process.env.PORT || 8080
app.use feathers.logger('dev')
app.use feathers.bodyParser()
app.use feathers.methodOverride()
app.use app.router

# Disable 304 caching
app.use (req, res, next) ->
	req.headers['if-none-match'] = 'no-match-for-this'
	next()

if app.get('env') is 'development'
	app.all '*', (req, res, next) ->
		res.header "Access-Control-Allow-Origin", "*"
		res.header "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"
		res.header "Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS"
		next()

# development only
if app.get('env') is 'development'
	console.log 'dev'
	#app.use feathers.errorHandler()
	app.use (err, req, res, next) ->
		res.send err.status, err

# production only
if app.get('env') is 'production'
	console.log 'prod'
	app.use (err, req, res, next) ->
		res.send err.status, err


### Routes ###

routes.setRoutes app, auth

# redirect all others to the index (HTML5 history)
# app.get '*', auth, routes.index
# last middleware will be 404 handler
app.use (req, res, next) ->
	# // 404 handler

### Start Server ###

app.listen(app.get('port'), () ->
	console.log 'Node server listening on port ' + app.get 'port'
);