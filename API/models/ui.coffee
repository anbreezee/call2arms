codes = require './helpers/codes'

exports.find = (cb) ->
	json = codes 200
	json.model = {}
	json.model.langs = require './json/lang.json'
	cb json
	
exports.get = (lang, cb) ->
	json = codes 200
	json.model = {}
	switch lang
		when 'en-US'
			json.model.interface = require './json/en-US.json'
			cb json
		when 'ru-RU'
			json.model.interface = require './json/ru-RU.json'
			cb json
		when 'en-US-full'
			json.model.interface = require './json/en-US-full.json'
			cb json
		when 'ru-RU-full'
			json.model.interface = require './json/ru-RU-full.json'
			cb json
		else
			cb codes 404