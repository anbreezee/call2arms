codes = require './helpers/codes'
db    = require './helpers/db'

exports.get = (callsign, cb) ->
	json = codes 200
	db.getRequest(callsign)
		.then(
			(model) ->
				json.model = model
				cb json
		)
		.fail(
			(reason) ->
				cb codes reason
		)

exports.create = (callsign, actionId, message, cb) ->
	json = codes 200
	db.addRequest(callsign, actionId, message)
		.then(
			(model) ->
				json.model = model
				cb json
		)
		.fail(
			(reason) ->
				cb codes reason
		)