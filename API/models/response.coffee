codes = require './helpers/codes'
db    = require './helpers/db'

exports.create = (callsign, authkey, actionId, message, _test, cb) ->
	json = codes 200
	json.model = {
		action: 'response'
		callsign: callsign
	}
	if actionId? then json.model.actionId = actionId
	if message? then json.model.message = message

	# For test users
	if _test?
		return cb json

	db.testAuthkey(callsign, authkey)
		.then(
			(ok) ->
				db.delRequest(callsign)
		)
		.then(
			(ok) ->
				cb json
		)
		.fail(
			(reason) ->
				cb codes reason
		)