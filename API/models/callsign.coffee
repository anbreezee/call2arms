codes	= require './helpers/codes'
db		= require './helpers/db'

exports.test = (callsign, _test, cb) ->
	json = codes 200

	# For test users
	if _test?
		return cb json

	db.testCallsign(callsign)
		.then(
			(ok) ->
				cb json
		)
		.fail(
			(reason) ->
				cb codes reason
		)

exports.get = (callsign, authkey, _test, cb) ->
	json = codes 200

	# For test users
	if _test?
		json.model = {
			callbackUrl: db.test.callbackUrl
			ttl: db.test.ttl
			request: {
				uid: "b8ae08e1cb669f2f2b29e26c161eb18b"
				action: "request"
				callsign: callsign
				actionId: "action-0"
				message: ""
    		}
		}
		return cb json

	db.testAuthkey(callsign, authkey)
		.then(
			(ok) ->
				return db.getCallsign(callsign)
		)
		.then(
			(model) ->
				json.model = model
				cb json
		)
		.fail(
			(reason) ->
				cb codes reason
		)

exports.create = (callsign, callbackUrl, _test, cb) ->
	json = codes 200

	# For test users
	if _test?
		json.model = {
			authkey: db.test.authkey
			ttl: db.test.ttl
		}
		return cb json

	db.setCallsign(callsign, callbackUrl)
		.then(
			(model) ->
				json.model = model
				cb json
		)
		.fail(
			(reason) ->
				cb codes reason
		)

exports.updateTtl = (callsign, authkey, _test, cb) ->
	json = codes 200

	# For test users
	if _test?
		json.model = {
			ttl: db.test.ttl
		}
		return cb json

	db.testAuthkey(callsign, authkey)
		.then(
			(ok) ->
				return db.setTTL(callsign)
		)
		.then(
			(ttl) ->
				json.model = { ttl: ttl }
				cb json
		)
		.fail(
			(reason) ->
				cb codes reason
		)

exports.updateCallback = (callsign, authkey, callbackUrl, _test, cb) ->
	json = codes 200

	# For test users
	if _test?
		json.model = { callbackUrl: db.test.callbackUrl }
		return cb json

	db.testAuthkey(callsign, authkey)
		.then(
			(ok) ->
				return db.setCallbackUrl(callsign, callbackUrl)
		)
		# After setting callbackUrl
		.then(
			(callbackUrl) ->
				json.model = { callbackUrl: callbackUrl }
				cb json
		)
		.fail(
			(reason) ->
				cb codes reason
		)

exports.remove = (callsign, authkey, _test, cb) ->
	json = codes 200
	json.specialForDevelopers = "Thank you"

	# For test users
	if _test?
		return cb json

	db.testAuthkey(callsign, authkey)
		.then(
			(ok) ->
				return db.delCallsign(callsign)
		)
		.then(
			(ok) ->
				cb json
		)
		.fail(
			(reason) ->
				cb codes reason
		)

exports.getCallbackUrl = (callsign, cb) ->
	json = codes 200
	db.getCallbackUrl(callsign)
		.then(
			(callbackUrl) ->
				json.model = { callbackUrl: callbackUrl }
				cb json
		)
		.fail(
			(reason) ->
				cb codes reason
		)