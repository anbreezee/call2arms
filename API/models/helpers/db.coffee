Q       = require 'q'
crypto	= require 'crypto'
redis	= require('redis').createClient()
hat     = require 'hat'

TTL          = 3600	# 1 hour
TTL_CALLBACK = 900	# 15 minutes
LN_AUTHKEY   = 16

exports.test = {
	callsign:    'JohnDoe'
	authkey:     '0123456789abcdef0123456789abcdef'
	callbackUrl: 'http://...'
	ttl:         TTL
}

###
ERRORS
###
normalErrCode = (code) ->
	if [403, 404, 409, 422, 500].indexOf(code) isnt -1
		return code
	return 500

###
COMMON
###

salt = exports.salt = (length) ->
	deferred = Q.defer()
	crypto.randomBytes length, (err, value) ->
		if err then deferred.reject 500
		else deferred.resolve (value.toString 'hex')
	deferred.promise

hmac = exports.hmac = (data) ->
	return crypto.createHmac("SHA256", "cfe5e0eb50898cd93fdf1fc10823b4be").update(data).digest('hex')

###
KEYS
###

getCallsignHMAC = (callsign) ->
	callsign = callsign.toLowerCase()
	return hmac callsign

keyAuthkey = (callsign) ->
	callsignHMAC = getCallsignHMAC callsign
	return 'callsign:' + callsignHMAC + ':authkey'

keyCallback = (callsign) ->
	callsignHMAC = getCallsignHMAC callsign
	return 'callsign:' + callsignHMAC + ':callbackUrl'

keyRequest = (callsign) ->
	callsignHMAC = getCallsignHMAC callsign
	return 'callsign:' + callsignHMAC + ':request'

keyNotExists = (key) ->
	deferred = Q.defer()
	redis.exists key, (err, value) ->
		if err
			return deferred.reject 500
		if value is 1
			return deferred.reject 409
		deferred.resolve 'ok'
	deferred.promise

keyExists = (key) ->
	deferred = Q.defer()
	redis.exists key, (err, value) ->
		if err
			return deferred.reject 500
		if value isnt 1
			return deferred.reject 404
		deferred.resolve 'ok'
	deferred.promise

###
CALLSIGN
###

delCallsignKeys = (callsign) ->
	deferred = Q.defer()
	redis.del (keyAuthkey callsign), (err, res) ->
		redis.del (keyCallback callsign), (err, res) ->
			redis.del (keyRequest callsign), (err, res) ->
				deferred.resolve 'ok'
	deferred.promise

exports.testCallsign = (callsign) ->
	deferred = Q.defer()
	keyExists(keyAuthkey callsign)
		# After testing callsign
		.then(
			(ok) ->
				deferred.resolve 'ok'
		)
		# If fails
		.fail(
			(reason) ->
				deferred.reject normalErrCode(reason)
		)
	deferred.promise

exports.getCallsign = (callsign) ->
	deferred = Q.defer()
	model = {}
	getCallbackUrl callsign
		# After getting callback URL
		.then(
			(callbackUrl) ->
				model.callbackUrl = callbackUrl
				return getTTL callsign
		)
		# After getting TTL
		.then(
			(ttl) ->
				model.ttl = ttl
				
				# Append request
				getRequest(callsign)
					.then(
						(request) ->
							model.request = request
							deferred.resolve model
					)
					.fail(
						(reason) ->
							if reason isnt 404
								return deferred.reject normalErrCode(reason)
							deferred.resolve model
					)
		)
		# If fails
		.fail(
			(reason) ->
				deferred.reject normalErrCode(reason)
		)
	deferred.promise

exports.setCallsign = (callsign, callbackUrl) ->
	deferred = Q.defer()
	model = {}
	keyNotExists(keyAuthkey callsign)
		# After testing exists
		.then(
			(ok) ->
				return delCallsignKeys callsign
		)
		# After removing callsign keys
		.then(
			(ok) ->
				return salt LN_AUTHKEY
		)
		# After generating authkey
		.then(
			(authkey) ->
				return setAuthkey callsign, authkey
		)
		# After set authkey
		.then(
			(authkey) ->
				model.authkey = authkey
				return setCallbackUrl callsign, callbackUrl
		)
		# After set callbackUrl
		.then(
			(callbackUrl) ->
				return setTTL callsign
		)
		# Set TTL
		.then(
			(ttl) ->
				model.ttl = ttl
				deferred.resolve model
		)
		# If fails
		.fail(
			(reason) ->
				if reason isnt 409
					delCallsignKeys callsign
				deferred.reject normalErrCode(reason)
		)
	deferred.promise
							

exports.delCallsign = (callsign) ->
	deferred = Q.defer()
	delCallsignKeys callsign
		# After removing callsign keys
		.then(
			(ok) ->
				deferred.resolve 'ok'
		)
		# If fails
		.fail(
			(reason) ->
				deferred.reject normalErrCode(reason)
		)
	deferred.promise

###
REQUEST
###

exports.addRequest = (callsign, actionId, message) ->
	deferred = Q.defer()
	rekey = keyRequest callsign

	model = {
		uid: hat()
		action: 'request'
		callsign: callsign
	}
	if actionId? then model.actionId = actionId
	if message? then model.message = message

	request = JSON.stringify model
	
	keyExists(keyAuthkey callsign)
		# After testing callsign exists
		.then(
			(ok) ->
				return keyNotExists(rekey)
		)
		# After testing request exists
		.then(
			(ok) ->
				return Q.ninvoke redis, 'setnx', rekey, request
		)
		# After set request
		.then(
			(value) ->
				if value is 0 then throw 409
				return Q.ninvoke redis, 'expire', rekey, TTL
		)
		# After set expire
		.then(
			(ok) ->
				deferred.resolve model
		)
		# If fails
		.fail(
			(reason) ->
				deferred.reject normalErrCode(reason)
		)
	deferred.promise

getRequest = exports.getRequest = (callsign) ->
	deferred = Q.defer()
	redis.get (keyRequest callsign), (err, value) ->
		if err
			return deferred.reject 500
		if not value?
			return deferred.reject 404
		model = JSON.parse value
		deferred.resolve model
	deferred.promise

exports.delRequest = (callsign) ->
	deferred = Q.defer()
	Q.ninvoke redis, 'del', keyRequest callsign
		.then(
			(value) ->
				if value is 0 then throw 404
				deferred.resolve 'ok'
		)
		.fail(
			(reason) ->
				deferred.reject normalErrCode(reason)
		)
	deferred.promise

###
AUTHKEY
###

exports.testAuthkey = (callsign, authkey) ->
	deferred = Q.defer()
	redis.get (keyAuthkey callsign), (err, value) ->
		if err
			return deferred.reject 500
		if not value?
			return deferred.reject 404
		if value isnt authkey
			return deferred.reject 403
		deferred.resolve value
	deferred.promise

setAuthkey = (callsign, authkey) ->
	deferred = Q.defer()
	redis.setnx (keyAuthkey callsign), authkey, (err, value) ->
		if err
			return deferred.reject 500
		if value is 0
			return deferred.reject 409
		deferred.resolve authkey
	deferred.promise

###
TTL
###

getTTL = (callsign) ->
	deferred = Q.defer()
	redis.ttl (keyAuthkey callsign), (err, value) ->
		if err
			return deferred.reject 500
		deferred.resolve value
	deferred.promise

setTTL = exports.setTTL = (callsign) ->
	###
	TODO set TTL for all keys of callsign (not key, cbkey only)
	###
	deferred = Q.defer()
	expire = Q.nbind redis.expire, redis
	expire((keyAuthkey callsign), TTL)
		.then(
			(ok) ->
				return expire((keyCallback callsign), TTL)
		)
		.then(
			(ok) ->
				expire((keyRequest callsign), TTL)
				return deferred.resolve TTL
		)
		.fail(
			(reason) ->
				deferred.reject normalErrCode(reason)
		)
	deferred.promise

###
CALLBACK
###

getCallbackUrl = exports.getCallbackUrl = (callsign) ->
	deferred = Q.defer()
	key = keyCallback callsign
	redis.get key, (err, value) ->
		if err
			return deferred.reject 500
		if not value?
			return deferred.reject 404
		deferred.resolve value
	deferred.promise

setCallbackUrl = exports.setCallbackUrl = (callsign, callbackUrl) ->
	deferred = Q.defer()
	key = keyCallback callsign
	Q.ninvoke redis, 'set', key, callbackUrl
		.then(
			(value) ->
				if value is 0 then throw 409
				deferred.resolve callbackUrl
		)
		.fail(
			(reason) ->
				deferred.reject normalErrCode(reason)
		)
	deferred.promise