module.exports = (code) ->
	switch code
		when 200
			return {status: 200, statusMessage: 'OK'};
		when 400
			return {status: 400, statusMessage: 'Bad Request'}
		when 403
			return {status: 403, statusMessage: 'Forbidden'}
		when 404
			return {status: 404, statusMessage: 'Not found'}
		when 409
			return {status: 409, statusMessage: 'Conflict'}
		when 422
			return {status: 422, statusMessage: 'Unprocessable Entity'}
		when 429
			return {status: 429, statusMessage: 'Too many request'}
		when 500
			return {status: 500, statusMessage: 'Internal server error'}
		else
			return {status: 500, statusMessage: ''}