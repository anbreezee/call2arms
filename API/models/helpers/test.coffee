module.exports = (vals) ->
	for own key, val of vals
		switch key
			when 'callsign'
				if not testCallsign val then return false
			when 'authkey'
				if not testAuthkey val then return false
			when 'callbackUrl'
				if not testCallbackUrl val then return false
			when 'actionId'
				if not testActionId val then return false
			when 'message'
				if not testMessage val then return false
	return true

testCallsign = (item) ->
	if not item? then return false
	correct = (/^[0-9a-zA-Z\(\)_\-!\$@]+$/.test item) and (6 <= item.length <= 64)
	return correct

testAuthkey = (item) ->
	if not item? then return false
	correct = (/^[0-9a-fA-F]+$/.test item) and (item.length is 32)
	return correct

testCallbackUrl = (item) ->
	if not item? then return true
	if item.length is 0 then return true
	correct = item.length <= 255
	if correct then correct = validateURL item
	return correct

testActionId = (item) ->
	if not item? then return false
	correct = item.length <= 64
	requests = []
	responses = []
	for i in [0..7] then requests.push('request-' + i)
	for i in [0..5] then responses.push('response-' + i)
	if item not in requests and item not in responses then correct = false
	return correct

testMessage = (item) ->
	if not item? then return true
	# correct = item.length <= 255
	correct = item.length is 0
	return correct

validateURL = (textval) ->
	urlregex = new RegExp("^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$")
	return urlregex.test textval
