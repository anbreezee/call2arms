faye = require 'faye'

secret = 'sj!k~lj4jgldfj0a9vjfsj4###oijgijoejboi5789jf0sgui043ivjoau49uvakfkfifghio';
URL = 'http://reply.call2arms.net/'

module.exports = (channel, json) ->
	client = new faye.Client URL
	client.addExtension {
		outgoing: (message, callback) ->
			message.ext = message.ext || {}
			message.ext.password = secret
			callback message
	}
	subscription = client.subscribe channel, (message) ->
	client.publish channel, json
	subscription.cancel()