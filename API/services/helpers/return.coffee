module.exports = (json, suppressResponseCodes, cb) ->
	status = if suppressResponseCodes? then 200 else json.status
	if status is 200 then return cb null, json
	return cb json