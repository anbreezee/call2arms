module.exports = (data, params, vals) ->
	for own key, value of vals
		if params? and params.query? and params.query[key]? then vals[key] = params.query[key]
		if data? and data[key]? then vals[key] = data[key]
	return vals