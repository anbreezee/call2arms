###
User interface
###

model        = require '../models/ui.coffee'
paramsHelper = require './helpers/params.coffee'
sendback     = require './helpers/return.coffee'

service = () ->

service.prototype.find = (params, cb) ->
	p = paramsHelper null, params, {suppressResponseCodes: null}
	model.find (json) ->
		return sendback json, p.suppressResponseCodes, cb

service.prototype.get = (lang, params, cb) ->
	p = paramsHelper null, params, {suppressResponseCodes: null}
	model.get lang, (json) ->
		return sendback json, p.suppressResponseCodes, cb

module.exports = service