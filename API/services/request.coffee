###
Request
###

model         = require '../models/request.coffee'
callback      = require '../schedule/callback.coffee'
codes         = require '../models/helpers/codes.coffee'
test          = require '../models/helpers/test.coffee'
paramsHelper  = require './helpers/params.coffee'
sendback      = require './helpers/return.coffee'
faye          = require './helpers/faye.coffee'

service = () ->

service.prototype.find = (params, cb) ->
	p = paramsHelper null, params, {suppressResponseCodes: null, method: null}
	if p.method?
		p.method = p.method.toLowerCase()
	if p.method is 'post'
		return this.create null, params, cb
	return sendback (codes 400), p.suppressResponseCodes, cb

service.prototype.get = (callsign, params, cb) ->
	p = paramsHelper null, params, {suppressResponseCodes: null}
	if not test {callsign: callsign}
		return sendback (codes 422), p.suppr	essResponseCodes, cb
	model.get callsign, (json) ->
		return sendback json, p.suppressResponseCodes, cb

service.prototype.create = (data, params, cb) ->
	p = paramsHelper data, params, {suppressResponseCodes: null, callsign: null, actionId: null, message: null}
	if not test {callsign: p.callsign, actionId: p.actionId, message: p.message}
		return sendback (codes 422), p.suppressResponseCodes, cb

	# Disable custom messages
	p.message = null

	model.create p.callsign, p.actionId, p.message, (json) ->
		if json.status isnt 200
			return sendback json, p.suppressResponseCodes, cb

		# Send to faye
		faye '/callsign/' + p.callsign.toLowerCase(), json.model

		# Callback queue
		callback.createJob json.model

		return sendback json, p.suppressResponseCodes, cb

module.exports = service