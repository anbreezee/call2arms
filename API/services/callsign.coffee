###
Callsign
###

model        = require '../models/callsign.coffee'
codes        = require '../models/helpers/codes.coffee'
test         = require '../models/helpers/test.coffee'
paramsHelper = require './helpers/params.coffee'
sendback     = require './helpers/return.coffee'
faye         = require './helpers/faye.coffee'

service = () ->

service.prototype.find = (params, cb) ->
	p = paramsHelper null, params, {suppressResponseCodes: null, method: null, _test: null}
	if p.method?
		p.method = p.method.toLowerCase()
	if p.method is 'post'
		return this.create null, params, cb
	sendback (codes 400), p.suppressResponseCodes, cb

service.prototype.get = (callsign, params, cb) ->
	p = paramsHelper null, params, {suppressResponseCodes: null, authkey: null, method: null, _test: null}
	if p.method?
		p.method = p.method.toLowerCase()
	if p.authkey?
		if not test {callsign: callsign, authkey: p.authkey}
			return sendback (codes 422), p.suppressResponseCodes, cb
		switch p.method
			when 'put'
				update callsign, null, params, callback
			when 'delete'
				remove callsign, params, callback
			else
				model.get callsign, p.authkey, p._test, (json) ->
					return sendback json, p.suppressResponseCodes, cb
	else
		if not test {callsign: callsign}
			return sendback (codes 422), p.suppressResponseCodes, cb
		model.test callsign, p._test, (json) ->
			return sendback json, p.suppressResponseCodes, cb

service.prototype.create = (data, params, cb) ->
	p = paramsHelper data, params, {suppressResponseCodes: null, callsign: null, callbackUrl: null, _test: null}
	if not test {callsign: p.callsign, callbackUrl: p.callbackUrl}
		return sendback (codes 422), p.suppressResponseCodes, cb
	model.create p.callsign, p.callbackUrl, p._test, (json) ->
		return sendback json, p.suppressResponseCodes, cb

service.prototype.update = (callsign, data, params, cb) ->
	p = paramsHelper data, params, {suppressResponseCodes: null, mode: null, authkey: null, _test: null}
	if not test {callsign: callsign, authkey: p.authkey}
		return sendback (codes 422), p.suppressResponseCodes, cb
	if p.mode is 'ttl'
		model.updateTtl callsign, p.authkey, p._test, (json) ->
			return sendback json, p.suppressResponseCodes, cb
	else if p.mode is 'callback'
		r = paramsHelper data, params, {callbackUrl: null}
		if not test {callbackUrl: r.callbackUrl}
			return sendback (codes 422), p.suppressResponseCodes, cb
		model.updateCallback callsign, p.authkey, r.callbackUrl, p._test, (json) ->
			return sendback json, p.suppressResponseCodes, cb
	else
		return sendback (codes 422), p.suppressResponseCodes, cb

service.prototype.remove = (callsign, params, cb) ->
	p = paramsHelper null, params, {suppressResponseCodes: null, authkey: null, _test: null}
	if not test {callsign: callsign, authkey: p.authkey}
		return sendback (codes 422), p.suppressResponseCodes, cb
	model.remove callsign, p.authkey, p._test, (json) ->

		# Send to faye
		json.model = { action: 'unregister' }
		faye '/callsign/' + callsign.toLowerCase(), json.model

		return sendback json, p.suppressResponseCodes, cb

module.exports = service