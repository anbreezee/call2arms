###
Response
###

model        = require '../models/response.coffee'
codes        = require '../models/helpers/codes.coffee'
test         = require '../models/helpers/test.coffee'
paramsHelper = require './helpers/params.coffee'
sendback     = require './helpers/return.coffee'
faye         = require './helpers/faye.coffee'

service = () ->

service.prototype.find = (params, cb) ->
	p = paramsHelper null, params, {suppressResponseCodes: null, method: null, _test: null}
	if p.method? then p.method = p.method.toLowerCase()
	switch p.method
		when 'post' then this.create null, params, cb
		else return sendback (codes 400), p.suppressResponseCodes, cb

service.prototype.create = (data, params, cb) ->
	p = paramsHelper data, params, {suppressResponseCodes: null, callsign: null, authkey: null, actionId: null, message: null, _test: null}
	if not test {callsign: p.callsign, authkey: p.authkey, actionId: p.actionId, message: p.message}
		return sendback (codes 422), p.suppressResponseCodes, cb

	# Disable custom messages
	p.message = null

	model.create p.callsign, p.authkey, p.actionId, p.message, p._test, (json) ->
		if json.status isnt 200
			return sendback json, p.suppressResponseCodes, cb

		# Send to faye
		faye '/callsign/' + p.callsign.toLowerCase(), json.model

		return sendback json, p.suppressResponseCodes, cb

module.exports = service