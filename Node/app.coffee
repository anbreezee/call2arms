feathers = require 'feathers'
path     = require 'path'

app = module.exports = feathers()
# app.configure feathers.socketio()

### Configuration ###

app.set 'port', process.env.PORT || 8008
app.use feathers.logger('dev')
app.use feathers.bodyParser()
app.use feathers.methodOverride()
app.use feathers.static(path.join(__dirname, 'public'))
app.use app.router

app.use feathers.vhost('m.call2arms.net', require('./app-mobile'))
app.use feathers.vhost('call2arms.net', require('./app-site'))
app.use feathers.vhost('www.call2arms.net', require('./app-site'))

### Start Server ###

app.listen(app.get('port'), () ->
	console.log 'Node server listening on port ' + app.get 'port'
);