feathers = require 'feathers'
routes   = require './routes'

app = module.exports = feathers()

app.set 'views', __dirname + '/views'
app.set 'view engine', 'jade'

### Secure credentials ###

passlock = false
username = 'root'
password = 'testpass123'

auth = []
if passlock
	auth = feathers.basicAuth(
		(user, pass) -> user is username and pass is password
		'Secret Area'
	)

# development only
if app.get('env') is 'development'
	console.log 'dev'
	#app.use feathers.errorHandler()
	app.use (err, req, res, next) ->
		res.send err, res.message

# production only
if app.get('env') is 'production'
	console.log 'prod'
	app.use (err, req, res, next) ->
		res.send err, res.message

### Routes ###

routes.setRoutes app, auth

# redirect all others to the index (HTML5 history)
# app.get '*', auth, routes.index
# last middleware will be 404 handler
app.use (req, res, next) ->
	# // 404 handler