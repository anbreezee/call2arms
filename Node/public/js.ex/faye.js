$(function() {
	$.faye = {
		client: null,
		subscription: null,
		onReceived: null,
		start: function(callsign) {
			if ($.faye.client == null) {
				$.faye.client = new Faye.Client('http://reply.call2arms.net/', { timeout: 120, retry: 5 });
			}
			if ($.faye.subscription != null) {
				$.faye.subscription.cancel();
			}
			$.faye.subscription = $.faye.client.subscribe('/callsign/' + callsign, function(data) {
				if ($.faye.onReceived != null) {
					$.faye.onReceived(data);
				}
			});
		},
		cancel: function() {
			if ($.faye.subscription != null) {
				$.faye.subscription.cancel();
			}
		}
	};
});