var app = angular.module('call2arms', ['call2arms.services', 'call2arms.controllers', 'ngAnimate', 'ngRoute', 'ngSanitize']);

app.config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.
			when('/', {
				controller: 'IndexCtrl',
				resolve: {
					ui: function(UiLoader) {
						return UiLoader();
					}
				},
				templateUrl: '/partials/index'
			}).when('/callsign/:callsign/:authkey', {
				controller: 'IndexCtrl',
				resolve: {
					ui: function(UiLoader) {
						return UiLoader();
					}
				},
				templateUrl: '/partials/index'
			}).otherwise({redirectTo: '/'});
	}
]);