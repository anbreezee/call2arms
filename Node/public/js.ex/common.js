$(function() {

	$.closeAlertOnClick = function($scope) {
		$('.alert').click(function() {
			$scope.error = false;
			$scope.$apply();
		});
	};

	$.constructMessage = function(pattern, message, callsign) {
		var str = pattern;
		str = str.replace(/%message%/, '<b>' + message + '</b>');
		str = str.replace(/%callsign%/, '<b>' + callsign + '</b>');
		return str;
	};

	$.ollist = {
		compare: function() { return true; },
		update: function() {
            if ($.ollist.compare()) {
                $.ollist.compact();
            } else {
                $.ollist.normal();
            }
        },
		compact: function() {
			$('ol > li').each(function() {
				$(this).addClass('compact');
			});
		},
		normal: function() {
			$('ol > li').each(function() {
				$(this).removeClass('compact');
			});
		}
	}

	$.getRequestMessageByAction = function($scope, actionId, defaultMessage) {
		if (defaultMessage) return defaultMessage;
	    var message = '';
	    for (var i in $scope.ui.request.actions) {
	        if ($scope.ui.request.actions[i].id == actionId) {
	            message = $scope.ui.request.actions[i].title;
	        }
	    }
	    return message;
	}

	$.getResponseMessageByAction = function($scope, actionId, defaultMessage) {
		if (defaultMessage) return defaultMessage;
	    var message = '';
	    for (var i in $scope.ui.response.actions) {
	        if ($scope.ui.response.actions[i].id == actionId) {
	            message = $scope.ui.response.actions[i].title;
	        }
	    }
	    return message;
	}

	$.getErrorMessage = function($scope, source, status, defaultMessage) {
        if (source['error_' + status]) return source['error_' + status];
        if ($scope.errors.general['error_' + status]) return $scope.errors.general['error_' + status];
		return defaultMessage;
	}
});