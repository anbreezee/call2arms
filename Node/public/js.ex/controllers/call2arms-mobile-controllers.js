var controllers = angular.module('call2arms.controllers', []);

controllers.controller('IndexCtrl', ['$scope', '$routeParams', '$http', 'ui',
	function($scope, $routeParams, $http, ui) {

        // Close alerts on clicks
		$.closeAlertOnClick($scope);

        // OL list
        $.ollist.compare = function() { return ($scope.processing || $scope.registered || $scope.error); };

        // Faye
        $.faye.onReceived = fayeMessageReceived;

        // Params
        $scope.callsign = $routeParams.callsign;
        $scope.ui = ui.interface;
        $scope.authkey = $routeParams.authkey;
        $scope.qrcode = false;

        // Test mobile
        $scope.mobile = true;
        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        } else {
            $('.web-mobile').addClass('show-phone');
            $scope.mobile = false;
        }

        // Stages
        $.stage = {
            default: function() {
                $scope.processing = false;
                $scope.registered = false;
                $scope.incoming = false;
                $scope.answered = false;
                hideQrCode();
                $.stage.always();
            },
            i12n: function() {
                $scope.processedMessage = $.constructMessage($scope.ui.messages.general.i12n, '', $scope.callsign);
                $scope.waitMessage = $scope.ui.messages.general.wait;
                $scope.processing = true;
                $.stage.always();
            },
            registering: function() {
            	$scope.processedMessage = $scope.ui.messages.registration.progress;
            	$scope.waitMessage = $scope.ui.messages.general.wait;
            	$scope.processing = true;
            	$scope.registered = false;
            	$.stage.always();
            },
            registered: function() {
		        $scope.processedMessage = $scope.ui.messages.registration.complete;
		        $scope.waitMessage = $scope.ui.messages.raider.wait;
            	$scope.processing = true;
            	$scope.registered = true;
            	showQrCode();
            	$.stage.always();
            },
            canceling: function() {
                $scope.processedMessage = $scope.ui.messages.cancellation.progress;
                $scope.waitMessage = $scope.ui.messages.general.wait;
                $scope.processing = true;
                $scope.registered = false;
                hideQrCode();
                $.stage.always();
            },
            canceled: function() {
                $scope.processedMessage = $scope.ui.messages.cancellation.complete;
                $scope.waitMessage = $scope.ui.messages.general.wait;
                $scope.processing = true;
                $scope.registered = false;
                $.stage.always();

                setTimeout(function() {
                    $scope.error = false;
                    $.stage.default();
                    $.stage.always();
                    $scope.$apply();
                }, 1000);
            },
            processing: function(actionId, message) {
                if (actionId == 'response-0') {
                    $scope.processedMessage = $scope.ui.messages.raider.accept.progress;
                } else {
                    $scope.processedMessage = $scope.ui.messages.raider.message.progress;
                }
                $scope.waitMessage = $scope.ui.messages.general.wait;
                $scope.processing = true;
                $scope.incoming = false;
                $.stage.always();
            },
            processed: function(actionId, message) {
                if (actionId == 'response-0') {
                    $scope.answerMessage = $scope.ui.messages.raider.accept.complete;
                } else {
                    message = $.getResponseMessageByAction($scope, actionId, message);
                    $scope.answerMessage = $.constructMessage($scope.ui.messages.raider.message.complete, message);
                }
                $scope.answered = true;
                $scope.incoming = false;
                $.stage.always();
            },
            incoming: function(actionId, message) {
                $scope.incomingMessage = $.getRequestMessageByAction($scope, actionId, message);
                $scope.incoming = true;
                $scope.answered = false;
                $.stage.always();
            },
            error: function(message, noPrefix) {
                $scope.error = true;
                if (!noPrefix) {
                    message = $scope.ui.titles.error + ': ' + message;
                }
                $scope.errorMessage = message;
                $scope.processing = false;
				$scope.registered = false;
				$.stage.always();

                setTimeout(function() {
                    $scope.error = false;
                    $.stage.always();
                    $scope.$apply();
                }, 4000);
            },
            always: function() {
                if ($scope.callsign && $scope.registered) {
                    $.faye.start($scope.callsign);
                }
                $.ollist.update();
            }
        };

		// Mainstream
        function refresh() {
    		$.stage.default();
    		if ($scope.callsign && $scope.authkey) {
    			$.stage.i12n();
    			$.requests.getCallsign($http, $scope.callsign, $scope.authkey).
    				then(function(data) {
    					$.stage.registered();
                        $.requests.getCurrent($http, $scope.callsign).
                            then(function(data) {
                                $.stage.processed(data.actionId, data.message);
                            }).
    						fail(function(data) {
    							if (data.status != 404) {
    								$.stage.error($.getErrorMessage($scope, $scope.ui.errors.request, data.status, data.statusMessage), false);
    			                }
    						});
    				}).
    				fail(function(data) {
    					$.stage.error($.getErrorMessage($scope, $scope.ui.errors.callsign, data.status, data.statusMessage), false);
    				});
    		}
        }
        refresh();

        // Actions
		$scope.register = function() {		
			$.stage.registering();	
			$.requests.registerCallsign($http, $scope.callsign).
				then(function(data) {
                    $scope.authkey = data.authkey;
					$.stage.registered();
				}).
				fail(function(data) {
                    $.stage.default();
					$.stage.error($.getErrorMessage($scope, $scope.ui.errors.callsign, data.status, data.statusMessage), false);
				});
		};
		$scope.cancel = function() {
            $.stage.canceling();
            $.requests.unregisterCallsign($http, $scope.callsign, $scope.authkey).
                then(function(data) {
                    $.faye.cancel();
                    $scope.authkey = '';
                    $.stage.canceled();
                }).
                fail(function(data) {
                    $.stage.default();
                    $.stage.error($.getErrorMessage($scope, $scope.ui.errors.callsign, data.status, data.statusMessage), false);
                });
		};
		$scope.acceptSummon = function() {
            var actionId = 'response-0';
            var message = '';
            $.stage.processing(actionId, message);
            $.requests.sendResponse($http, $scope.callsign, $scope.authkey, 'response-0', '').
                then(function(data) {
                    $.stage.processed(actionId, message);
                }).
                fail(function(data) {
                    $.stage.error($.getErrorMessage($scope, $scope.ui.errors.response, data.status, data.statusMessage), false);
                    refresh();
                });
		};
		$scope.sendMessage = function(actionId, message) {
            $.stage.processing(actionId, message);
            $.requests.sendResponse($http, $scope.callsign, $scope.authkey, actionId, message).
                then(function(data) {
                    $.stage.processed(actionId, message);
                }).
                fail(function(data) {
                    $.stage.error($.getErrorMessage($scope, $scope.ui.errors.response, data.status, data.statusMessage), false);
                    refresh();
                });
		};
        $scope.closeAnswer = function() {
            refresh();
        };

        // QR code
		function showQrCode() {
			if ($scope.mobile) return;
			$qrcode = $('#qrcode');
			$qrcode.html('');
			var url = 'http://m.call2arms.net/#/callsign/' + $scope.callsign + '/' + $scope.authkey;
			$qrcode.qrcode({render: 'canvas', width: 200, height: 200, color: '#000', text: url});
			$qrcode.append('<div>Instant switch to mobile</div>');
            $scope.qrcode = true;
            $qrcode.show();
		}

		function hideQrCode() {
			if ($scope.mobile) return;
			$qrcode = $('#qrcode');
            $scope.qrcode = false;
		}

		// Faye Events
        function fayeMessageReceived(data) {
            if (data['action'] == 'request') {
            	$.stage.incoming(data['actionId'], data['message']);
                $.ionSound.play("horn");
                $scope.$apply();
            } else if (data['action'] == 'response') {
                $.stage.processed(data['actionId'], data['message']);
                $scope.$apply();
            } else if (data['action'] == 'unregister') {
                if ($scope.registered) {
                    $.stage.default();
                }
                $.faye.cancel();
                $scope.authkey = '';
                $scope.$apply();
            }
        }

        // Preloader
		$scope.isViewLoading = false;
		$scope.$on('$routeChangeStart', function() {
			$scope.isViewLoading = true;
		});
		$scope.$on('$routeChangeSuccess', function() {
			$scope.isViewLoading = false;
		});
	}
]);