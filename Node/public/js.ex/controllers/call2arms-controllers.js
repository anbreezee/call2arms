var controllers = angular.module('call2arms.controllers', []);

controllers.controller('IndexCtrl', ['$scope', '$routeParams', '$http', 'ui',
	function($scope, $routeParams, $http, ui) {

        // Close alerts on clicks
        $.closeAlertOnClick($scope);

        // OL list
        $.ollist.compare = function() { return ($scope.processing || $scope.error); };

        // Faye
        $.faye.onReceived = fayeMessageReceived;

        // Params
        $scope.callsign = $routeParams.callsign;
        $scope.ui = ui.interface;

        // Stages
        $.stage = {
            default: function() {
                $scope.processing = false;
                // $scope.incoming = false;
                $.stage.always();
            },
            i12n: function() {
                $scope.processedMessage = $.constructMessage($scope.ui.messages.general.i12n, '', $scope.callsign);
                $scope.waitMessage = $scope.ui.messages.general.wait;
                $scope.processing = true;
                $.stage.always();
            },
            processing: function(actionId, message) {
                if (actionId == 'request-0') {
                    $scope.processedMessage = $scope.ui.messages.raidleader.summon.progress;
                } else {
                    $scope.processedMessage = $scope.ui.messages.raidleader.message.progress;
                }
                $scope.waitMessage = $scope.ui.messages.general.wait;
                $scope.processing = true;
                $.stage.always();
            },
            processed: function(actionId, message) {
                if (actionId == 'request-0') {
                    $scope.processedMessage = $.constructMessage($scope.ui.messages.raidleader.summon.complete, '', $scope.callsign);
                } else {
                    message = $.getRequestMessageByAction($scope, actionId, message);
                    $scope.processedMessage = $.constructMessage($scope.ui.messages.raidleader.message.complete, message, $scope.callsign);
                }
                $scope.waitMessage = $scope.ui.messages.raidleader.wait;
                $scope.processing = true;
                $.stage.always();
            },
            incoming: function(actionId, message) {
                $scope.incomingMessage = $.getResponseMessageByAction($scope, actionId, message);
                $scope.incoming = true;
                $.stage.always();
            },
            error: function(message, noPrefix, isRed) {
                $scope.error = true;
                if (!noPrefix) {
                    message = $scope.ui.titles.error + ': ' + message;
                }
                if (isRed) {
                    $scope.errorClass = 'alert-danger';
                } else {
                    $scope.errorClass = 'alert-info';
                }
                $scope.errorMessage = message;
                $scope.processing = false;
                $.stage.always();

                setTimeout(function() {
                    $scope.error = false;
                    $.stage.always();
                    $scope.$apply();
                }, 4000);
            },
            always: function() {
                if ($scope.callsign) {
                    $.faye.start($scope.callsign);
                }
                $.ollist.update();
            }
        };

        // Mainstream
        function refresh() {
            $.stage.default();
            if ($scope.callsign) {
                $.stage.i12n();
                $.requests.testCallsign($http, $scope.callsign).
                    then(function(data) {
                        $.requests.getCurrent($http, $scope.callsign).
                            then(function(data) {
                                $.stage.processed(data.actionId, data.message);
                            }).
                            fail(function(data) {
                                if (data.status != 404) {
                                    $.stage.error($.getErrorMessage($scope, $scope.ui.errors.request, data.status, data.statusMessage), false, true);
                                } else {
                                    $.stage.default();
                                    $.faye.start($scope.callsign);
                                }
                            });
                    }).
                    fail(function(data) {
                        $.stage.error($.getErrorMessage($scope, $scope.ui.errors.callsign, data.status, data.statusMessage), false, true);
                    });
            }
        }
        refresh();

        // Actions
		$scope.summonUser = function() {
            var actionId = 'request-0';
            var message = '';
            $.stage.processing(actionId, message);
            $.requests.sendRequest($http, $scope.callsign, 'request-0', '').
                then(function(data) {
                    $.stage.processed(data.actionId, data.message);
                }).
                fail(function(data) {
                    if (data.status == 409) {
                        $.stage.error($scope.ui.messages.raidleader.alreadySent, true, false);
                    }
                    else {
                        $.stage.error($.getErrorMessage($scope, $scope.ui.errors.request, data.status, data.statusMessage), false, true);
                    }
                    refresh();
                });
		};
		$scope.sendMessage = function(actionId, message) {
            $.stage.processing(actionId, message);
            $.requests.sendRequest($http, $scope.callsign, actionId, message).
                then(function(data) {
                    $.stage.processed(data.actionId, data.message);
                }).
                fail(function(data) {
                    if (data.status == 409) {
                        $.stage.error($scope.ui.messages.raidleader.alreadySent, true, false);
                    }
                    else {
                        $.stage.error($.getErrorMessage($scope, $scope.ui.errors.request, data.status, data.statusMessage), false, true);
                    }
                    refresh();
                });
		};
        $scope.closeIncoming = function() {
            $scope.incoming = false;
            refresh();
        };

        // Faye Events
        function fayeMessageReceived(data) {
            if (data['action'] == 'request') {
                $.stage.processed(data['actionId'], data['message']);
                $scope.$apply();
            } else if (data['action'] == 'response') {
                $.stage.incoming(data['actionId'], data['message']);
                $scope.$apply();
            } else if (data['action'] == 'unregister') {
                $.stage.default();
                $.faye.cancel();
                $scope.$apply();
            }
        }

        // Preloader
        $scope.isViewLoading = false;
        $scope.$on('$routeChangeStart', function() {
            $scope.isViewLoading = true;
        });
        $scope.$on('$routeChangeSuccess', function() {
            $scope.isViewLoading = false;
        });
	}
]);