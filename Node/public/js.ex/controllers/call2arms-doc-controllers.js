var controllers = angular.module('call2arms.controllers', []);

controllers.controller('ApiCtrl', ['$scope', '$routeParams', '$location',
	function($scope, $routeParams, $location) {

		$.closeAlertOnClick();

		$('.section').click(function() {
			var expanded = $(this).attr("data-expanded");
			if (expanded == '0') {
				$(this).attr("data-expanded", '1');
				$(this).parent().find('.desc').show(400);
				$(this).parent().find('.spec').show(400);
				$(this).parent().find('.mark').removeClass('glyphicon-expand').addClass('glyphicon-collapse-up');

				$codes = $(this).parent().find('.code');
				$codes.each(function() {
					if($(this).text() == '') {
						$(this).text('Loading JSON...');
						var json_url = $(this).attr('data-json');
						var mode = $(this).attr('data-mode');
						var data = $.parseJSON($(this).attr('data-data'));
						updateCode($(this), json_url, mode, data);
					}
				});

			} else {
				$(this).attr("data-expanded", '0');
				$(this).parent().find('.mark').removeClass('glyphicon-collapse-up').addClass('glyphicon-expand');
				$(this).parent().find('.spec').hide(400);
				$(this).parent().find('.desc').hide(400);
			}
		});

		$('.toc_item').click(function() {
			location.href = '#' + $(this).attr('data-url');
			return false;
		});

		$('.toc_item').removeClass('active');
		$('.toc_item').each(function() {
			var data_url = $(this).attr('data-url');
			if (data_url === $location.path()) {
				$(this).addClass('active');
				$(this).blur();
			}
		});

		$scope.isViewLoading = false;
		$scope.$on('$routeChangeStart', function() {
			$scope.isViewLoading = true;
		});
		$scope.$on('$routeChangeSuccess', function() {
			$scope.isViewLoading = false;
		});

        var updateCode = function($element, json_url, mode, data) {
        	$.ajax({
        		url: 'http://api.call2arms.net' + json_url,
        		type: mode,
        		data: data,
        		success: function(data) {
                    var jsonString = JSON.stringify(data, null, 4);
                    jsonString = hljs.highlight('json', jsonString).value;
                    $element.html(jsonString);
        		}
        	})
        }
	}
]);