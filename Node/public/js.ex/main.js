require.config({
	baseUrl: '/js',
	paths: {
		// JQuery & Common files
		'jquery':           'jquery',
		'requests':         'requests',
		'common':           'common',

		// Bootstrap and fonts
		'boostrap':         'bootstrap.min',
		'edgefonts-pt':     '//use.edgefonts.net/pt-sans-narrow:n4,n7:all',
		'edgefonts-open':   '//use.edgefonts.net/open-sans-condensed:n3',

		// AngularJS basic
		'angular':          'angular',
		'angular-animate':  'angular-animate',
		'angular-route':    'angular-route',
		'angular-resource': 'angular-resource',
		'angular-sanitize': 'angular-sanitize',

		// AngularJS logic
		'app':              'app/call2arms',
		'services':         'services/call2arms-services',
		'controllers':      'controllers/call2arms-controllers',
		'modules':          'modules',

		// Faye
		'faye':             '//reply.call2arms.net/client',
		'faye-common':      'faye'
	},
	shim: {
		'jquery':           { 'exports': '$' },
		'requests':         { deps:['jquery'] },
		'common':           { deps:['jquery'] },
		'boostrap':         { deps:['jquery'] },
		'edgefonts-pt':     { deps:['jquery'] },
		'edgefonts-open':   { deps:['jquery'] },

		'angular':          { 'exports': 'angular', deps:['jquery'] },
		'angular-animate':  { deps:['angular'] },
		'angular-route':    { deps:['angular'] },
		'angular-resource': { deps:['angular'] },
		'angular-sanitize': { deps:['angular'] },
		'app':              { 'exports': 'app',         deps:['angular', 'angular-animate', 'angular-route', 'angular-resource', 'angular-sanitize', 'jquery'] },
		'services':         { 'exports': 'services',    deps:['app'] },
		'controllers':      { 'exports': 'controllers', deps:['services', 'requests', 'common'] },
		'modules':          { deps:['controllers'] },

 		'faye':             { 'exports': 'Faye' },
		'faye-common':      { deps:['faye', 'jquery'] }
	}
});

var js = ['jquery', 'requests', 'common', 'boostrap', 'edgefonts-pt', 'edgefonts-open',
		'angular', 'angular-animate', 'angular-route', 'angular-resource', 'angular-sanitize',
		'app', 'services', 'controllers', 'modules',
 		'faye', 'faye-common'];

require(js, function () {
	$(function () {
		angular.bootstrap(document, ['call2arms']);
	});
});