$(function() {

	$.requests = new Object();

	$.requests.testCallsign = function($http, callsign) {
		var d = $.Deferred();
		url = 'http://api.call2arms.net/v1/callsign/' + callsign;
		$http({method: 'GET', url: url}).
			success(function(data, status, headers, config) {
				d.resolve(data);
			}).
			error(function(data, status, headers, config) {
				d.reject(data);
			});
		return d.promise();
	}

	$.requests.getCallsign = function($http, callsign, authkey) {
		var d = $.Deferred();
		url = 'http://api.call2arms.net/v1/callsign/' + callsign +  '?authkey=' + authkey;
		$http({method: 'GET', url: url}).
			success(function(data, status, headers, config) {
				d.resolve(data);
			}).
			error(function(data, status, headers, config) {
				d.reject(data);
			});
		return d.promise();
	}

	$.requests.registerCallsign = function($http, callsign) {
		var d = $.Deferred();
		url = 'http://api.call2arms.net/v1/callsign';
		$http({method: 'POST', url: url, data: {callsign: callsign, callbackUrl: ''}}).
			success(function(data, status, headers, config) {
				d.resolve(data);
			}).
			error(function(data, status, headers, config) {
				d.reject(data);
			});
		return d.promise();
	}

	$.requests.unregisterCallsign = function($http, callsign, authkey) {
		var d = $.Deferred();
		url = 'http://api.call2arms.net/v1/callsign/' + callsign + '?authkey=' + authkey;
		$http({method: 'DELETE', url: url, data: {}}).
			success(function(data, status, headers, config) {
				d.resolve(data);
			}).
			error(function(data, status, headers, config) {
				d.reject(data);
			});
		return d.promise();
	}

	$.requests.getCurrent = function($http, callsign) {
		var d = $.Deferred();
		url = 'http://api.call2arms.net/v1/request/' + callsign;
		$http({method: 'GET', url: url}).
			success(function(data, status, headers, config) {
				d.resolve(data);
			}).
			error(function(data, status, headers, config) {
				d.reject(data);
			});
		return d.promise();
	}

	$.requests.sendRequest = function($http, callsign, actionId, message) {
		var d = $.Deferred();
		url = 'http://api.call2arms.net/v1/request/';
		$http({method: 'POST', url: url, data: {callsign: callsign, actionId: actionId, message: message}}).
			success(function(data, status, headers, config) {
				d.resolve(data);
			}).
			error(function(data, status, headers, config) {
				d.reject(data);
			});
		return d.promise();
	}

	$.requests.sendResponse = function($http, callsign, authkey, actionId, message) {
		var d = $.Deferred();
		url = 'http://api.call2arms.net/v1/response/';
		$http({method: 'POST', url: url, data: {callsign: callsign, authkey: authkey, actionId: actionId, message: message}}).
			success(function(data, status, headers, config) {
				d.resolve(data);
			}).
			error(function(data, status, headers, config) {
				d.reject(data);
			});
		return d.promise();
	}

});