require.config({
	baseUrl: '/js',
	paths: {
		// Nanobar
		'nanobar':          'nanobar.min',

		// JQuery & Common files
		'jquery':           'jquery/jquery-1.11.0.min',
		'requests':         'requests.min',
		'common':           'common.min',

		// Social
		'fb-sdk':           'social/fb/sdk.min',

		// Modernizr
		'modernizr-svg':    'modernizr/svg.min',

		// Bootstrap and fonts
		'bootstrap':         'bootstrap/bootstrap.min',

		// AngularJS basic
		'angular':          'angular/angular.min',
		'angular-animate':  'angular/angular-animate.min',
		'angular-route':    'angular/angular-route.min',
		'angular-resource': 'angular/angular-resource.min',
		'angular-sanitize': 'angular/angular-sanitize.min',

		// AngularJS logic
		'app':              'angular/app/call2arms-doc.min',
		'services':         'angular/services/call2arms-doc-services.min',
		'controllers':      'angular/controllers/call2arms-doc-controllers.min',
		'modules':          'angular/modules.min',

		// Highlight
		'hljs':             'hljs/highlight.pack.min'
	},
	shim: {
		'nanobar':          { 'exports': 'Nanobar' },

		'jquery':           { 'exports': '$' },

		'modernizr-svg':    { exports: 'Modernizr', deps: ['jquery'] },
		'requests':         { deps: ['jquery'] },
		'fb-sdk':           { deps: ['jquery'] },
		'common':           { deps: ['jquery'], deps: ['modernizr-svg', 'fb-sdk'] },

		'bootstrap':        { deps: ['jquery'] },

		'angular':          { 'exports': 'angular', deps: ['jquery'] },
		'angular-animate':  { deps: ['angular'] },
		'angular-route':    { deps: ['angular'] },
		'angular-resource': { deps: ['angular'] },
		'angular-sanitize': { deps: ['angular'] },

		'app':              { 'exports': 'app', deps: ['angular', 'angular-animate', 'angular-route', 'angular-resource', 'angular-sanitize', 'jquery'] },
		'services':         { 'exports': 'services', deps: ['app'] },
		'controllers':      { 'exports': 'controllers', deps: ['services', 'requests', 'common'] },
		'modules':          { deps: ['controllers'] },

		'hljs':             { 'exports': 'hljs', deps: ['jquery'] }
	}
});

var js = [
	['nanobar'],
	['jquery'],
	['modernizr-svg', 'requests', 'fb-sdk', 'common'],
	['bootstrap'],
	['angular', 'angular-animate', 'angular-route', 'angular-resource', 'angular-sanitize'],
	['app', 'services', 'controllers', 'modules'],
	['hljs']
];

var loadScript = function(key) {
	if (key == js.length) {
		$(function () {
			nanobar.go(100);
			angular.bootstrap(document, ['call2arms']);
		});
		return;
	}
	require(js[key], function () {
		if (key == 0) {
			nanobar = new Nanobar({ bg: '#d42026', id: 'nano' });
		}
		nanobar.go((key * 100.0) / js.length);
		loadScript(key+1);
	});
}

loadScript(0);