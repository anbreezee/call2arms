$(function() {
	var aboutWrap = {
		show: function() {
			$('#aboutWrap').fadeIn(200);
		},
		hide: function() {
			$('#aboutWrap').fadeOut(200);
			$.cookie('before_start_popup', 'hidden', {expires: 24 * 365 * 10, path: '/', domain: '.call2arms.net'});
			var intro = introJs();
			intro.setOption('keyboardNavigation', true);
			intro.start();
		}
	}

	$('.closeAbout').click(function() { aboutWrap.hide(); });
	$("#about").click(function(e) { e.stopPropagation(); });
	$('#menu-about').click(function() { aboutWrap.show(); });

	if ($.cookie('before_start_popup') != 'hidden') {
		aboutWrap.show();
	}
});