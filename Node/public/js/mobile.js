require.config({
	baseUrl: '/js',
	paths: {
		// Nanobar
		'nanobar':          'nanobar.min',

		// JQuery & Common files
		'jquery':           'jquery/jquery-1.11.0.min',
		'cookie':           'cookie/jquery.cookie.min',
		'lang':             'jquery/jquery.polyglot.language.switcher.min',
		'requests':         'requests.min',
		'homescreen':       'addtohomescreen.min',
		'common':           'common.min',

		// Social
		'fb-sdk':           'social/fb/sdk.min',

		// Modernizr
		'modernizr-svg':    'modernizr/svg.min',

		// Bootstrap and fonts
		'bootstrap':         'bootstrap/bootstrap.min',

		// AngularJS basic
		'angular':          'angular/angular.min',
		'angular-animate':  'angular/angular-animate.min',
		'angular-route':    'angular/angular-route.min',
		'angular-resource': 'angular/angular-resource.min',
		'angular-sanitize': 'angular/angular-sanitize.min',
		'ui-bootstrap':     'bootstrap/ui-bootstrap-tpls-0.11.0.min',

		// AngularJS logic
		'app':              'angular/app/call2arms-mobile.min',
		'services':         'angular/services/call2arms-mobile-services.min',
		'controllers':      'angular/controllers/call2arms-mobile-controllers.min',
		'modules':          'angular/modules.min',

		// Faye
		'faye':             'faye/client',
		'faye-common':      'faye/faye.min',

		// QR
		'qr':               'jquery/jquery.qrcode-0.7.0.min',

		// Sounds
		'ion':              'sounds/ion.sound.min',
		'sounds':           'sounds/sounds.min'
	},
	shim: {
		'nanobar':          { 'exports': 'Nanobar' },

		'jquery':           { 'exports': '$' },
		'cookie':           { deps: ['jquery'] },
		'lang':             { deps: ['jquery'] },

		'modernizr-svg':    { exports: 'Modernizr', deps: ['jquery'] },
		'requests':         { deps: ['jquery'] },
		'homescreen':       { deps: ['jquery'] },
		'fb-sdk':           { deps: ['jquery'] },
		'common':           { deps: ['jquery', 'cookie', 'modernizr-svg', 'homescreen', 'fb-sdk'] },

		'bootstrap':        { deps: ['jquery'] },

		'angular':          { 'exports': 'angular', deps: ['jquery', 'cookie'] },
		'angular-animate':  { deps: ['angular'] },
		'angular-route':    { deps: ['angular'] },
		'angular-resource': { deps: ['angular'] },
		'angular-sanitize': { deps: ['angular'] },
		'ui-bootstrap':     { deps: ['bootstrap', 'angular'] },

		'app':              { 'exports': 'app',         deps: ['angular', 'angular-animate', 'angular-route', 'angular-resource', 'angular-sanitize', 'jquery'] },
		'services':         { 'exports': 'services',    deps: ['app'] },
		'controllers':      { 'exports': 'controllers', deps: ['services', 'requests', 'common'] },
		'modules':          { deps: ['controllers'] },

 		'faye':             { 'exports': 'Faye' },
		'faye-common':      { deps: ['faye', 'jquery'] },

		'qr':               { deps: ['jquery'] },

		'ion':              { deps: ['jquery'] },
		'sounds':           { deps: ['ion'] }
	}
});

var js = [
	['nanobar'],
	['jquery', 'cookie', 'lang'],
	['modernizr-svg', 'requests', 'homescreen', 'fb-sdk', 'common'],
	['bootstrap'],
	['angular', 'angular-animate', 'angular-route', 'angular-resource', 'angular-sanitize', 'ui-bootstrap'],
	['app', 'services', 'controllers', 'modules'],
	['faye', 'faye-common'],
	['qr'],
	['ion', 'sounds']
];

var loadScript = function(key) {
	if (key == js.length) {
		$(function () {
			nanobar.go(100);
			angular.bootstrap(document, ['call2arms']);
		});
		return;
	}
	require(js[key], function () {
		if (key == 0) {
			nanobar = new Nanobar({ bg: '#d42026', id: 'nano' });
		}
		nanobar.go((key * 100.0) / js.length);
		loadScript(key+1);
	});
}

loadScript(0);