var services = angular.module('call2arms.services', ['ngResource']);

services.factory('Ui', ['$resource',
	function($resource) {
		return $resource('http://api.call2arms.net/v1/ui/:lang');
	}
]);

services.factory('UiLoader', ['Ui', '$q',
	function(Ui, $q) {
		return function() {
			var delay = $q.defer();
			var lang = $.cookie('locale');
			if (lang != 'en-US' && lang != 'ru-RU') lang = 'en-US';
            Ui.get({lang: lang}, function(ui) {
				delay.resolve(ui);
			}, function() {
				delay.reject('Unable to fetch message actions');
			});
			return delay.promise;
		}
	}
]);