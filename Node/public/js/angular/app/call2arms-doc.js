var app = angular.module('call2arms', ['call2arms.services', 'call2arms.controllers', 'ngRoute', 'ngSanitize']);

app.config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.
			when('/', {
				controller: 'ApiCtrl',
				templateUrl: '/partials/doc_intro'
			}).when('/api-ui', {
				controller: 'ApiCtrl',
				templateUrl: '/partials/doc_api_ui_'
			}).when('/api-callsign', {
				controller: 'ApiCtrl',
				templateUrl: '/partials/doc_api_callsign_'
			}).when('/api-callback', {
				controller: 'ApiCtrl',
				templateUrl: '/partials/doc_api_callback_'
			}).when('/api-response', {
				controller: 'ApiCtrl',
				templateUrl: '/partials/doc_api_response_'
			}).when('/api-ontheside', {
				controller: 'ApiCtrl',
				templateUrl: '/partials/doc_api_ontheside_'
			}).when('/examples', {
				controller: 'ApiCtrl',
				templateUrl: '/partials/doc_examples'
			}).otherwise({redirectTo: '/'});
	}
]);