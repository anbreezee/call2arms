angular.module('call2arms').directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if(event.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter, {'event': event});
                });
                event.preventDefault();
            }
        });
    };
});

controllers.controller('LoadCtrl', ['$scope', '$routeParams',
	function($scope, $routeParams) {
		$scope.isViewLoading = false;
		$scope.$on('$routeChangeStart', function() {
			$scope.isViewLoading = true;
		});
		$scope.$on('$routeChangeSuccess', function() {
			$scope.isViewLoading = false;
		});
	}
]);
