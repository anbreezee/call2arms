$(function() {

	setTimeout(function() {
		var addtohome = addToHomescreen({
			autostart: false,
			modal: true,
			lifespan: 3600 * 12
		});
		addtohome.show();
	}, 2000);

	// Change SVG to Fallback SRC if SVG doesn't supported
	if (!Modernizr.svg) {
		$("img.svg").each(function() {
			$(this).attr('src', $(this).attr('data-fallback-src'));
		});
	}

	$.closeAlertOnClick = function($scope) {
		$("[data-hide]").on("click", function(){
			$.lazy.unset('error-close');
			$scope.error = false;
			$.stage.always();
			$scope.$apply();
    	});
	};

	$.constructMessage = function(pattern, message, callsign) {
		var str = pattern;
		str = str.replace(/%message%/, '<b>' + message + '</b>');
		str = str.replace(/%callsign%/, '<b>' + callsign + '</b>');
		return str;
	};

	$.ollist = {
		compare: function() { return true; },
		update: function() {
            if ($.ollist.compare()) {
                $.ollist.compact();
            } else {
                $.ollist.normal();
            }
        },
		compact: function() {
			$('ol > li').each(function() {
				$(this).addClass('compact');
			});
		},
		normal: function() {
			$('ol > li').each(function() {
				$(this).removeClass('compact');
			});
		}
	}

	$.getRequestMessageByAction = function($scope, actionId, defaultMessage) {
		if (defaultMessage) return defaultMessage;
	    var message = '';
	    for (var i in $scope.ui.request.actions) {
	        if ($scope.ui.request.actions[i].id == actionId) {
	            message = $scope.ui.request.actions[i].title;
	        }
	    }
	    return message;
	}

	$.getResponseMessageByAction = function($scope, actionId, defaultMessage) {
		if (defaultMessage) return defaultMessage;
	    var message = '';
	    for (var i in $scope.ui.response.actions) {
	        if ($scope.ui.response.actions[i].id == actionId) {
	            message = $scope.ui.response.actions[i].title;
	        }
	    }
	    return message;
	}

	$.getErrorMessage = function($scope, source, status, defaultMessage) {
        if (source['error_' + status]) return source['error_' + status];
        if ($scope.ui.errors.general['error_' + status]) return $scope.ui.errors.general['error_' + status];
		return defaultMessage;
	}

	// Lazy
	$.lazy = {
		timers: [],
		set: function($scope, timer, func, interval) {
			this.unset(timer);
			if (interval == 0) {
				func();
			} else {
				var id = setTimeout(function() { func(); $scope.$apply(); }, interval);
				this.timers[timer] = {id: id};
			}
		},
		unset: function(timer) {
			if (this.timers[timer] != undefined) {
				clearTimeout(this.timers[timer].id);
			}
		}
	};

	// Lang
	$switcher = $('#polyglotLanguageSwitcher');
	if ($switcher.length) {
		$switcher.polyglotLanguageSwitcher({
			effect: 'fade',
			testMode: false,
			onChange: function(evt){
				$.removeCookie('locale');
				if (evt.selectedItem == 'ru') {
					$.cookie('locale', 'ru-RU', { expires: 24 * 365 * 10, path: '/', domain: '.call2arms.net'});
				} else {
					$.cookie('locale', 'en-US', { expires: 24 * 365 * 10, path: '/', domain: '.call2arms.net'});
				}
				location.reload(false);
			}
		});
	}

	//var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
	var iOS = ( navigator.userAgent.match(/(iPhone)/g) ? true : false );
	var android = ( navigator.userAgent.match(/(Android)/g) ? true : false );
	var chrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());

	if (iOS) {
		$bar = $('#iOsWarnBar');
		if ($bar.length) {
			$bar.show();
		}
	}
	if (android && chrome) {
		navigator.standalone = navigator.standalone || (screen.height <= document.documentElement.clientHeight);
		if (!navigator.standalone) {
			$('#android-add-to-homescreen').show();
		}
	}

	$('#fb-share-btn').click(function() {
		FB.init({
			appId: '652475614833110',
			xfbml: true,
			version: 'v2.0'
		});
		FB.ui({
			method: 'share',
			href: 'http://call2arms.net'
		}, function(response){});
	});
});