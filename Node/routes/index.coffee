parseCookies = (request) ->
	list = {}
	rc = request.headers.cookie
	rc && rc.split(';').forEach (cookie) ->
		parts = cookie.split '='
		list[parts.shift().trim()] = unescape(parts.join '=')
	return list

index = (req, res) ->
	cookies = parseCookies req
	if cookies.locale is 'ru-RU'
		res.render 'index-ru'
	else
		res.render 'index'

pressKit = (req, res) ->
	cookies = parseCookies req
	if cookies.locale is 'ru-RU'
		res.render 'press-kit-ru'
	else
		res.render 'press-kit'

agreement = (req, res) ->
	cookies = parseCookies req
	if cookies.locale is 'ru-RU'
		res.render 'developer-agreement-ru'
	else
		res.render 'developer-agreement'

forDevelopers = (req, res) ->
	cookies = parseCookies req
	if cookies.locale is 'ru-RU'
		res.render 'for-developers-ru'
	else
		res.render 'for-developers'

doc = (req, res) ->
	res.render 'doc'

partials = (req, res) ->
  	name = req.params.name
  	res.render 'partials/' + name

exports.setRoutes = (app, auth) ->
	app.get '/', auth, index
	app.get '/doc', auth, doc
	app.get '/press-kit', auth, pressKit
	app.get '/developer-agreement', auth, agreement
	app.get '/for-developers', auth, forDevelopers
	app.get '/partials/:name', auth, partials