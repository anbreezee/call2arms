parseCookies = (request) ->
	list = {}
	rc = request.headers.cookie
	rc && rc.split(';').forEach (cookie) ->
		parts = cookie.split '='
		list[parts.shift().trim()] = unescape(parts.join '=')
	return list

index = (req, res) ->
	cookies = parseCookies req
	if cookies.locale is 'ru-RU'
		res.render 'mobile-ru'
	else
		res.render 'mobile'

partials = (req, res) ->
  	name = req.params.name
  	res.render 'partials/mobile/' + name

exports.setRoutes = (app, auth) ->
	app.get '/', auth, index
	app.get '/partials/:name', auth, partials